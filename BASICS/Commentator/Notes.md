
BINARY
DECIMAL
0 0 0 0
0 0 0 1
0 0 1 0
0 0 1 1
0 1 0 0
0 1 0 1
0 1 1 0
0 1 1 1
1 0 0 0
1 0 0 1
1 0 1 0
1 0 1 1
1 1 0 0
1 1 0 1
1 1 1 0
1 1 1 1

Data Types - family of data that computer will be encoding. We need to know wich family this data belongs to to be able to decide how many binary digits (memory) is required to encode each of the data.
Numbers
Integers
Floating point - 2.16
Characters
Single character - ‘c’, ‘a’, ‘d’
Multiple characters - Strings - “Commentator” = ‘c’ + ‘o’ + ‘m’ + ‘m’ + ‘e’ + ‘n’ + ‘t’ + ‘a’ + ‘t’ + ‘o’ + ‘r’
So, when you are doing mathematical operations, you need to ensure that the data type is not character, but numbers.

Python - is not a strongly type defined language. - weakness of python.
Means - if i have a variable say, a. I need not to declare the family of the data (data type) to which a belongs.

```python
A = input (“Enter your marks1”)
100
B = input (“Enter your marks2”
200
X = A + B
Print (X) =  300 # This does not happen.
Output = 100200
```

```mermaid
graph LR;
  A([Datatype])-->B([Characters])
  A-->C([Numbers])-->D([Integer])
  C-->E([Float])
```

Python by default considers all entries as character strings.
And arithmetic operators like “+”, is overloaded to work with character strings by concatenating the strings provided.
But for other mathematical operations, it will cause a type definition error.

In C++
Int a = 10

>> So to be able to do arithmetic operations on entries in python, we need to explicitly change the data type to a numerical data type -  integer or floating. This is done by a process known as explicit type casting. That is done as follows -
