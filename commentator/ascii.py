from colorama import Fore

p = input()
#if input is more than one character exit as invalid input
if len(p) > 1 :
    print (Fore.RED + "BRO" + " " + Fore.CYAN + "invalid input" + Fore.BLUE + " " + "pls enter one character at a time!!!")
    exit() 
#converting into ascii
p = ord(p)
#determining the family of the character input
if p >= 48 and p <=57:
    print ("numerical input")
elif p >= 97 and p <=122:
    print ("small case letter")
elif p >= 65 and p <=90:
    print ("upper case letter")
else :
    print ("special case character")
