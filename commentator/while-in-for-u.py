# fruits = ["apple", "banana", "cherry"]
# for x in fruits:
#   print(x)

# for x in range(1,10) :
#     print (x)

# for x in ["delhi", "mumbai", "kolkata", "madras"] :
#    print (x)


# sum = 0
# for i in range (5) :
#     print ("enter the", i, "th number")
#     sum += int(input())
# print("sum of the numbers entered is : ", sum)
# print ("mean of the numbers entered is :", sum/5)

# print (i)
# print ("mean of the numbers entered is= ", sum/i)


# sum = 0
# print("Enter the Value of n: ")
# n = int(input())
# print("Enter " + str(n) + " Numbers: ")
# for i in range(n):
#     num = int(input())
#     sum = sum+num
# print("Sum of " + str(n) + " Numbers = " + str(sum))

# index = 0
# while (index <= 10):
#     index+=1
#     print ("This is shout out number", index)

# i=0
# while (True) :
#     i+=1
#     a = input ()
#     if a == "stopit":
#         break
#     if a == "chodh de yaar":
#         continue 
#     print ("Hi there!!. This is my shoutout number", i, " ", a)


# sum = 0
# prod = 1
# # for x in [1,2,3,4]:
# # for x in range (1,5)
# for x in range(2,100,2):
#     sum = sum + x
#     prod = prod * x 
#     print (x )
# print (sum, prod)


# s = "lumberjack"
# t = ("and", "I'm", "okay")

# for x in s: 
#     print (x)

# for x in t: 
#     print (x)

# T = [(1,2), (3,4), (5,6)]
# for (a,b) in T:
#     print (a,b)

# T = [(1, "Subhrodeep", "De", 16, "Amboli", "Mumbai"),
#      (2, "Shalini", "Mhavat", 15, "KachodiGali", "Indore"),
#      (3, "Ridhima", "Gupta", 17, "Ranikhet", "Himachal")
#     ]
# for (pid, fname, secname, age, add, city) in T :
#     print (pid, fname, secname, age, add, city)

# D = {'a':1, 'b' :2, 'c' :3}
# for key in D:
#     print (key, '=>', D[key])

# D = {'a':1, 'b' :2, 'c' :3}
# for (key,value) in D.items(): # items() is a method inside the class Dictionary which returns a list of tuples containing all th key value pairs of he dictionary 
#     print (key, value)

#tuple is generic enough to act as an empty hand to receive data stream from a collection of diverse data as shwown below
count = 1
for ((a,b),c) in [([1,2],3), ['XY',6], ((77,88),99)]:
    print ("iteration number ", count);
    print (a,b,c)
    count = count +1


