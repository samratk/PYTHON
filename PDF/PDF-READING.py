import PyPDF2

# reading the pdf file
pdf_object = open('../../MPFOWA/FINANCIAL-REPORTS/BANK-STATEMENTS/2020-2021/JUNE2020.pdf', 'rb')
pdf_reader = PyPDF2.PdfFileReader(pdf_object)

# Number of pages in the PDF file
page_object = []
total_pages = pdf_reader.numPages
print (total_pages)
# get a certain page's text

for i in range(total_pages):
    page_object.append(pdf_reader.getPage(i))

# Extract text from the page_object
for i in range(total_pages):
    print(page_object[i].extractText())
